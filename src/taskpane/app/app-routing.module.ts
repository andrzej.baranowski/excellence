import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthComponent } from "./auth/auth.component";
import { MainUiComponent } from "./mainInterface/mainui.component";

const routes: Routes = [
    {
        path:'',
        redirectTo: 'auth',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        component: AuthComponent
    },
    {
        path: 'main',
        component: MainUiComponent
    },
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule { }