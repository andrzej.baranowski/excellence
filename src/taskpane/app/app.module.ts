import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import AppComponent from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from "./app-routing.module";
import { RouteReuseStrategy, RouterModule } from "@angular/router";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { MainUiComponent } from "./mainInterface/mainui.component";
import { AuthComponent } from "./auth/auth.component";

@NgModule({
  declarations: [AppComponent, AuthComponent, MainUiComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule, IonicModule.forRoot(),
    RouterModule, AppRoutingModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent],
  entryComponents: [AuthComponent, MainUiComponent]
})
export default class AppModule { }
