import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ComponentRef, ElementRef, Inject, NgZone, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Wrapper } from '../../../utils/wrapper';
import { AuthService } from './auth.service';
const template = require('./auth.component.html');

@Component({
    selector: 'app-login',
    template
})

export class AuthComponent implements OnInit {
    private email: string;
    private password: string;
    private loginSuccessfull: boolean = true;

    constructor(
        @Inject(AuthService) private authService: AuthService,
        @Inject(Router) private router: Router
    ) { }

    ngOnInit() { }

    onLoginButtonClick(){
        this.authService.logIn(this.email, this.password).subscribe(success => {
            if (!success){
                this.logInFailed();
                return;
            }
            let path = "/main"            
            this.router.navigateByUrl(path);
        }, state => {
            this.logInFailed();
        });
    }

    private logInFailed(){
        this.loginSuccessfull = false;
        setTimeout( 
            ()=>this.loginSuccessfull=!this.loginSuccessfull, 5000
        );
    }
}