import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { catchError, switchMap, tap } from 'rxjs/operators';

export class Token {
    "access_token": string;
    "expires_in": number;
    "not-before-policy": number;
    "refresh_expires_in": number;
    "refresh_token": string;
    "scope": string | "profile email";
    "session_state": string;
    "token_type": string | "bearer";
    "email"?: string
}

@Injectable({ providedIn: 'root' })
export class AuthService {
    get url() { return "https://id.rmedison.com/auth/realms/rmedison/protocol/openid-connect/token"; }
    private refreshToken: string;
    private tokenSubject = new BehaviorSubject<Token>(null);

    get token() {
        return this.tokenSubject;
    }

    constructor(
        @Inject(HttpClient) private http: HttpClient
    ) { }

    logIn(email: string, password: string) {
        let params = new URLSearchParams();
        params.set("client_id", "rmedison-client")
        params.set("username", email)
        params.set("password", password)
        params.set("grant_type", "password");

        return this.http.post<Token>(this.url,
            params.toString(),
            {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
            })
            .pipe(
                tap(response => {
                    console.log(response);
                    
                    response.email = email;
                    this.tokenSubject.next(response);
                }),
                switchMap(response => {
                    return of(true);
                }), catchError(err => {
                    return of(false);
                }));
    }
}


/**
andrzej.baranowski@rmedison.com

*/