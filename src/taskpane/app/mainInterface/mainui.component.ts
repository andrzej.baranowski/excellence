import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ComponentRef, ElementRef, Inject, Injectable, NgZone, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from '@microsoft/office-js-helpers';
import { Wrapper } from '../../../utils/wrapper';
import { AuthService, Token } from '../auth/auth.service';
import { MainUiService } from './mainui.service';
const template = require('./mainui.component.html');

@Component({
    selector: 'app-holder',
    template
})

export class MainUiComponent implements OnInit, OnDestroy {
    private tokenSubscription: Subscription;
    private email: string = "";
    private token: Token;

    constructor(
        @Inject(MainUiService) private mainUiService: MainUiService,
        @Inject(AuthService) private authService: AuthService,
        @Inject(HttpClient) private http: HttpClient
    ) { }

    ngOnInit() {
        this.tokenSubscription = this.authService.token.subscribe(token => {
            this.email = token.email;
            this.token = token;
        });
    }

    ngOnDestroy() {
        this.tokenSubscription.unsubscribe();
    }

    onCreateSheetsButtonClick() {
        this.mainUiService.createOrReplaceContractorsDataWorksheet();
    }

    onFillContractorsDataClick() {
        let headers = new HttpHeaders()
            .set('Authorization', 
            `${this.token.token_type}${this.token.access_token}`);

        // this.http.post(

        // ).subscribe();

    }
}