import { Injectable } from "@angular/core";
import { HeaderRowElement, ExcelElement } from "../../../utils/excelElement";
import { Wrapper } from "../../../utils/wrapper";

@Injectable({ providedIn: "root" })
export class MainUiService {
    private contractorsDataWorksheetName = "Dane - kontrahenci";

    createOrReplaceContractorsDataWorksheet() {
        let contractorsDetailsHeaderPairs: ExcelElement[][] = [
            [
                new HeaderRowElement("Dane Twoich kontrahentów", 21)
            ], [
                new HeaderRowElement("Imię 2"),
                new HeaderRowElement("Imię 1"),
                new HeaderRowElement("Imię 3"),
                new HeaderRowElement("Nazwisko 1"),
                new HeaderRowElement("Nazwisko 2"),
                new HeaderRowElement("Ulica"),
                new HeaderRowElement("Numer budynku"),
                new HeaderRowElement("Numer lokalu"),
                new HeaderRowElement("Kod pocztowy"),
                new HeaderRowElement("Miasto"),
                new HeaderRowElement("Numer wynajmowanego lokalu"),
                new HeaderRowElement("Osoba 1: Imię 1"),
                new HeaderRowElement("Osoba 1: Imię 2"),
                new HeaderRowElement("Osoba 1: Imię 3"),
                new HeaderRowElement("Osoba 1: Nazwisko 1"),
                new HeaderRowElement("Osoba 1: Nazwisko 2"),
                new HeaderRowElement("Osoba 2: Imię 1"),
                new HeaderRowElement("Osoba 2: Imię 2"),
                new HeaderRowElement("Osoba 2: Imię 3"),
                new HeaderRowElement("Osoba 2: Nazwisko 1"),
                new HeaderRowElement("Osoba 2: Nazwisko 2")

            ]
        ]

        let transfersDatesHeaderPairs: ExcelElement[][] = [
            [
                new HeaderRowElement("2019-01", 2),
                new HeaderRowElement("2020-01", 2),
                new HeaderRowElement("2020-02", 2),
                new HeaderRowElement("2020-03", 2),
                new HeaderRowElement("2020-04", 2),
                new HeaderRowElement("2020-05", 2),
                new HeaderRowElement("2020-06", 2),
                new HeaderRowElement("2020-07", 2),
                new HeaderRowElement("2020-08", 2),
                new HeaderRowElement("2020-09", 2),
                new HeaderRowElement("2020-10", 2),
                new HeaderRowElement("2020-11", 2),
                new HeaderRowElement("2020-12", 2),
                new HeaderRowElement("2020", 2)
            ]
        ]

        transfersDatesHeaderPairs.push(
            ExcelElement.convertToMultiplicatedArray<ExcelElement[]>(
                [
                    new HeaderRowElement("Winien"),
                    new HeaderRowElement("Ma"),
                ],
                transfersDatesHeaderPairs[0].length));







        //TODO: set `contractorsDetailsHeaderPairs[0].size` to `contractorsDetailsHeaderPairs[1]` sum of sizes









        Wrapper.callExcelMethod(
            (context) => {
                const worksheetsCollection = context.workbook.worksheets;
                return worksheetsCollection.getItemOrNullObject(this.contractorsDataWorksheetName).load();
            },
            (context, foundWorksheet: Excel.Worksheet) => {
                let contractorsDetailsWorksheet: Excel.Worksheet;
                if (foundWorksheet.isNullObject) {
                    contractorsDetailsWorksheet = context.workbook.worksheets.add(this.contractorsDataWorksheetName);
                } else {
                    foundWorksheet.visibility = Excel.SheetVisibility.visible;
                    foundWorksheet.delete();
                    contractorsDetailsWorksheet = context.workbook.worksheets.add(this.contractorsDataWorksheetName);
                    console.log("Recreated sheet");
                }
                contractorsDetailsWorksheet.activate();

                var lastCell = this.createHeader(contractorsDetailsWorksheet, 0, 0, contractorsDetailsHeaderPairs);

                this.createHeader(contractorsDetailsWorksheet, 0, lastCell.lastColumn, transfersDatesHeaderPairs);

                return contractorsDetailsWorksheet;
            }
        );
    }

    fillContractorsData(data: string[]){
        Wrapper.callExcelMethod(
            (context)=>{

            }
            
            );
            
    }



    private createHeader(worksheet: Excel.Worksheet, startingRow: number, startingColumn: number, headers: ExcelElement[][]) {
        let currentRow = startingRow;
        let maxColumn = startingColumn;
        headers.forEach((headersRow) => {
            let currentColumn = startingColumn
            headersRow.forEach(element => {
                let range = worksheet.getRangeByIndexes(currentRow, currentColumn, 1,
                    element.size);

                range.merge();
                range.values = [ExcelElement.convertToMultiplicatedArray(element.value, element.size)];
                range.format.autofitColumns();
                range.format.autofitRows();
                if (element.properties) {
                    range.setCellProperties([ExcelElement.convertToMultiplicatedArray(element.properties, element.size)]);
                }
                currentColumn += (element.size) ? element.size : 1;
            })
            maxColumn = Math.max(maxColumn, currentColumn);
            currentRow += 1;
        });
        return {
            lastRow: currentRow,
            lastColumn: maxColumn
        };
    }
}