export class ExcelElement {
    size: number = 1;
    properties?: Excel.SettableCellProperties;

    constructor(public value: string = "", size?: number, properties?: Excel.SettableCellProperties) {
        this.size = (size)? size : 1;
        this.properties = properties;
    }

    static convertToMultiplicatedArray<T>(elem: T, size: number | undefined) {
        let array = [];
        for (let i = 0; i < ((size) ? size : 1); i++) {
            if (Array.isArray(elem)) {
                elem.forEach(element => {
                    array.push(element);
                })
            } else {
                array.push(elem);
            }
        }
        return array;
    }
}

export class HeaderRowElement extends ExcelElement {
    constructor(public value: string = "", size?: number, public properties?: Excel.SettableCellProperties){
        super(value, size, properties);
        this.properties = (properties)?properties : {
            format: {
                fill: {
                    color: "Blue"
                },
                font: {
                    color: "White",
                    bold: true
                },
                borders:{
                    left:{
                        color: "White",
                        style: "Continuous"
                    },
                    right:{
                        color: "White",
                        style: "Continuous"
                    },
                    top:{
                        color: "White",
                        style: "Continuous"
                    },
                    bottom:{
                        color: "White",
                        style: "Continuous"
                    }
                }
            }
        };
    }
}