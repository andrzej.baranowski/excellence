/// <reference path="../../node_modules/@types/office-js/index.d.ts" />

export class Wrapper {
  static callExcelMethod(
    asyncFunc?: { (context: Excel.RequestContext): any },
    syncFunc?: { (context: Excel.RequestContext, asyncReturnValue?: any): any }) {
      this.createPromiseExcelMethod(asyncFunc, syncFunc).call(this);
  }

  static createPromiseExcelMethod(
    asyncFunc?: { (context: Excel.RequestContext): any },
    syncFunc?: { (context: Excel.RequestContext, asyncReturnValue?: any): any }): Function {
    return () => {
      return new Promise(async (resolve, reject) => {
        await Excel.run(async context => {
          let returnVal = null;
          if (asyncFunc != null) {
            returnVal = asyncFunc(context);
          }
          return context.sync().then(() => {
            if (syncFunc != null) {
              returnVal = syncFunc(context, returnVal);
            }
            resolve(returnVal);
          });
        }).catch(receivedError => {
          console.log("An error occoured while executing Excel related code.");
          console.log(receivedError);
          reject(receivedError);
        });
      });
    }
  }
}